# Menetrend Székesfehérvár

This is an application that contains the bus schedule of Székesfehérvár served from an offline database.

## Features

- Map
- Stops
- Trips

## Installation

- Install pods in project library
- Run project in xcode
- Profit

## Installation

- Install pods in project library
- Run project in xcode
- Profit

## How to update database

The database needs to be manually created from the GTFS file.

- Extract the gtfs file
- Run GTFSParser.py in the resources folder
- Put the created file into an sqlite database
- Save the database and replace the file in the project with the new one

## How to send notifications

- Notifications are provided from a private hosting right now
- Change the rest api url to your own api and use it that way or delete this part if you do not want to send notifications

## Meta

Barnabas Lorincz – barnipro@gmail.com

Distributed under the MIT license. See ``LICENSE`` for more information.
