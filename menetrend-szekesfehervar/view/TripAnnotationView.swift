//
//  TripAnnotationView.swift
//  menetrend-szekesfehervar
//
//  Created by Barnabas Lorincz on 2020. 02. 07..
//  Copyright © 2020. Barnabás Lőrincz. All rights reserved.
//

import MapKit

class TripAnnotationView: MKAnnotationView {
  override var annotation: MKAnnotation? {
    willSet {
      guard let tripAnnotation = newValue as? TripAnnotation else {return}
      canShowCallout = true
      calloutOffset = CGPoint(x: 0, y: 0)
        let detailLabel = UILabel()
        detailLabel.numberOfLines = 0
        detailLabel.font = detailLabel.font.withSize(12)
        detailLabel.text = tripAnnotation.subtitle
        detailCalloutAccessoryView = detailLabel
    }
  }
}
