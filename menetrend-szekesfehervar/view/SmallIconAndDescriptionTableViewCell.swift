//
//  SmallIconAndDescriptionTableViewCell.swift
//  menetrend-szekesfehervar
//
//  Created by Barnabas Lorincz on 2020. 03. 19..
//  Copyright © 2020. Barnabás Lőrincz. All rights reserved.
//

import UIKit

class SmallIconAndDescriptionTableViewCell: UITableViewCell {
    
    @IBOutlet weak var iconCount: UILabel!
    @IBOutlet weak var imageIcon: UIImageView!
    @IBOutlet weak var leftLabel: UILabel!
    @IBOutlet weak var rightLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
