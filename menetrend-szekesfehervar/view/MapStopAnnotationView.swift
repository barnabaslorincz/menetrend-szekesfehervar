//
//  ArtworkView.swift
//  menetrend-szekesfehervar
//
//  Created by Lőrincz Barnabás on 2019. 12. 25..
//  Copyright © 2019. Barnabás Lőrincz. All rights reserved.
//

import MapKit

class MapStopAnnotationView: MKAnnotationView {
  override var annotation: MKAnnotation? {
    willSet {
      guard let mapStopModel = newValue as? StopAnnotation else {return}
      canShowCallout = true
      calloutOffset = CGPoint(x: 0, y: 0)
        let detailLabel = UILabel()
        detailLabel.numberOfLines = 0
        detailLabel.font = detailLabel.font.withSize(12)
        detailLabel.text = mapStopModel.subtitle
        detailCalloutAccessoryView = detailLabel
        rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
    }
  }
    
}


