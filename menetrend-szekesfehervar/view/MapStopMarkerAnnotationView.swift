//
//  MapStopMarkerAnnotationView.swift
//  menetrend-szekesfehervar
//
//  Created by Barnabas Lorincz on 2020. 02. 13..
//  Copyright © 2020. Barnabás Lőrincz. All rights reserved.
//

import MapKit

class MapStopMarkerAnnotationView: MKMarkerAnnotationView {
  override var annotation: MKAnnotation? {
    willSet {
      guard let mapStopModel = newValue as? StopAnnotation else {return}
      canShowCallout = true
      calloutOffset = CGPoint(x: 0, y: 0)
        let detailLabel = UILabel()
        detailLabel.numberOfLines = 0
        detailLabel.font = detailLabel.font.withSize(12)
        detailLabel.text = mapStopModel.subtitle
        detailCalloutAccessoryView = detailLabel
        rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
        configure(with: mapStopModel)
    }
  }
    
}

private extension MapStopMarkerAnnotationView {
    func configure(with annotation: MKAnnotation) {
        guard annotation is StopAnnotation else { fatalError("Unexpected annotation type: \(annotation)") }
        markerTintColor = .blue
        glyphImage = UIImage(named: "bus-white")
        clusteringIdentifier = String(describing: MapStopMarkerAnnotationView.self)
        displayPriority = .required
    }
}
