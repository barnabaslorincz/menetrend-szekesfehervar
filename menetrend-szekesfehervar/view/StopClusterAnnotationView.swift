//
//  ClusterAnnotationView.swift
//  menetrend-szekesfehervar
//
//  Created by Barnabas Lorincz on 2020. 02. 13..
//  Copyright © 2020. Barnabás Lőrincz. All rights reserved.
//

import MapKit

/// - Tag: ClusterAnnotationView
class StopClusterAnnotationView: MKAnnotationView {
    
    override init(annotation: MKAnnotation?, reuseIdentifier: String?) {
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
        collisionMode = .circle
        centerOffset = CGPoint(x: 0, y: -10) // Offset center point to animate better with marker annotations
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /// - Tag: CustomCluster
    override func prepareForDisplay() {
        super.prepareForDisplay()
        
        if let cluster = annotation as? MKClusterAnnotation {
            let totalStops = cluster.memberAnnotations.count
            image = drawStopCount(count: totalStops)
            displayPriority = .required
        }
    }

    private func drawStopCount(count: Int) -> UIImage {
        return drawRatio(0, to: count, wholeColor: #colorLiteral(red: 0.2910000086, green: 0.3569999933, blue: 0.5389999747, alpha: 1))
    }

    private func drawRatio(_ fraction: Int, to whole: Int, wholeColor: UIColor?) -> UIImage {
        let renderer = UIGraphicsImageRenderer(size: CGSize(width: 40, height: 40))
        return renderer.image { _ in
            // Fill full circle with wholeColor
            wholeColor?.setFill()
            UIBezierPath(ovalIn: CGRect(x: 0, y: 0, width: 40, height: 40)).fill()

            // Fill inner circle with white color
            #colorLiteral(red: 0.2910000086, green: 0.3569999933, blue: 0.5389999747, alpha: 1).setFill()
            UIBezierPath(ovalIn: CGRect(x: 8, y: 8, width: 24, height: 24)).fill()

            // Finally draw count text vertically and horizontally centered
            let attributes = [ NSAttributedString.Key.foregroundColor: UIColor.white,
                               NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 20)]
            let text = "\(whole)"
            let size = text.size(withAttributes: attributes)
            let rect = CGRect(x: 20 - size.width / 2, y: 20 - size.height / 2, width: size.width, height: size.height)
            text.draw(in: rect, withAttributes: attributes)
        }
    }
}
