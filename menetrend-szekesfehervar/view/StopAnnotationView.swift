//
//  StopAnnotationView.swift
//  menetrend-szekesfehervar
//
//  Created by Barnabas Lorincz on 2020. 02. 13..
//  Copyright © 2020. Barnabás Lőrincz. All rights reserved.
//

import MapKit

class StopAnnotationView: MKMarkerAnnotationView {
    override var annotation: MKAnnotation? {
      willSet {
        guard let mapStopModel = newValue as? StopAnnotation else {return}
        canShowCallout = true
        calloutOffset = CGPoint(x: 0, y: 0)
          let detailLabel = UILabel()
          detailLabel.numberOfLines = 0
          detailLabel.font = detailLabel.font.withSize(12)
          detailLabel.text = mapStopModel.subtitle
          detailCalloutAccessoryView = detailLabel
          rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
      }
    }

    static let ReuseID = "stopAnnotation"

    /// - Tag: ClusterIdentifier
    override init(annotation: MKAnnotation?, reuseIdentifier: String?) {
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
        clusteringIdentifier = "stop"
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func prepareForDisplay() {
        super.prepareForDisplay()
        displayPriority = .required
        markerTintColor = #colorLiteral(red: 0.2910000086, green: 0.3569999933, blue: 0.5389999747, alpha: 1)
        glyphImage = #imageLiteral(resourceName: "bus-white")
    }
}
