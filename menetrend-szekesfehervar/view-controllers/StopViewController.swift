//
//  StopViewController.swift
//  menetrend-szekesfehervar
//
//  Created by Barnabas Lorincz on 2020. 01. 30..
//  Copyright © 2020. Barnabás Lőrincz. All rights reserved.
//

import UIKit

class StopTableViewCell: UITableViewCell {
    
    @IBOutlet weak var stopName: UILabel!
    @IBOutlet weak var stopDistance: UILabel!
    
}

class StopViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var allStopResources: [StopResource] = []
    var otherStopResources: [StopResource] = []
    var favouriteStopResources: [StopResource] = []
    var filteredStopResources: [StopResource] = []
    let searchController = UISearchController(searchResultsController: nil)
    let favouritesService: FavouritesService = FavouritesService(key: "favouriteStops")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = NSLocalizedString("MENU_STOPS", comment: "")
        // 1
                searchController.searchResultsUpdater = self as UISearchResultsUpdating
                // 2
                searchController.obscuresBackgroundDuringPresentation = false
                // 3
                searchController.searchBar.placeholder = NSLocalizedString("SEARCH_STOPS", comment: "")
                // 4
                navigationItem.searchController = searchController
                // 5
                definesPresentationContext = true
                searchController.searchBar.delegate = self as UISearchBarDelegate
                
                let notificationCenter = NotificationCenter.default
                notificationCenter.addObserver(forName: UIResponder.keyboardWillChangeFrameNotification,
                                               object: nil, queue: .main) { (notification) in
                                                self.handleKeyboard(notification: notification) }
                notificationCenter.addObserver(forName: UIResponder.keyboardWillHideNotification,
                                               object: nil, queue: .main) { (notification) in
                                                self.handleKeyboard(notification: notification) }
                
            }
            
            override func viewWillAppear(_ animated: Bool) {
              super.viewWillAppear(animated)
                loadResources()
                if let indexPath = tableView.indexPathForSelectedRow {
                  tableView.deselectRow(at: indexPath, animated: true)
                }
            }
            
            var isSearchBarEmpty: Bool {
              return searchController.searchBar.text?.isEmpty ?? true
            }
            
            var isFiltering: Bool {
              let searchBarScopeIsFiltering = searchController.searchBar.selectedScopeButtonIndex != 0
              return searchController.isActive && (!isSearchBarEmpty || searchBarScopeIsFiltering)
            }
            
            func filterContentForSearchText(_ searchText: String) {
              filteredStopResources = allStopResources.filter { (stopResource: StopResource) -> Bool in
                
                if isSearchBarEmpty {
                    return true;
                } else {
                    return stopResource.stopName.lowercased().starts(with: searchText.lowercased())
                }
              }
              tableView.reloadData()
            }
            
            func handleKeyboard(notification: Notification) {
              // 1
              guard notification.name == UIResponder.keyboardWillChangeFrameNotification else {
                view.layoutIfNeeded()
                return
              }
              
              guard
                let info = notification.userInfo,
                let keyboardFrame = info[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue
                else {
                  return
              }
              
              // 2
                _ = keyboardFrame.cgRectValue.size.height
              UIView.animate(withDuration: 0.1, animations: { () -> Void in
                self.view.layoutIfNeeded()
              })
            }
    
    
    
    func loadResources() {
        allStopResources = StopDbHelper().getAllStops()
        let favourites = favouritesService.getFavourites()
        favouriteStopResources.removeAll()
        otherStopResources.removeAll()
        for stop in allStopResources {
            if favourites.contains(stop.stopName) {
                favouriteStopResources.append(stop)
                stop.favourite = true
            }
            else {
                otherStopResources.append(stop)
                stop.favourite = false
            }
        }
        tableView.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
      guard
        segue.identifier == "ShowStopDetailSegue",
        let indexPath = tableView.indexPathForSelectedRow,
        let detailViewController = segue.destination as? StopDetailViewController
        else {
          return
      }
          let stop: StopResource
          if isFiltering {
              stop = filteredStopResources[indexPath.row]
          } else {
              if (indexPath.section == 0 && favouriteStopResources.count > 0) {
                  stop = favouriteStopResources[indexPath.row]
              }
              else {
                  stop = otherStopResources[indexPath.row]
              }
          }
        detailViewController.stopId = nil
        detailViewController.stopName = stop.stopName
        detailViewController.sender = .list
        detailViewController.favourite = stop.favourite
    }

}

extension StopViewController: UITableViewDataSource, UITableViewDelegate {
  func tableView(_ tableView: UITableView,
                 numberOfRowsInSection section: Int) -> Int {
    if isFiltering {
      return filteredStopResources.count
    }
    if (section == 0 && favouriteStopResources.count > 0) {
        return favouriteStopResources.count
    }
    else {
        return otherStopResources.count
    }
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      let cell = tableView.dequeueReusableCell(withIdentifier: "StopCell", for: indexPath)
                          as! StopTableViewCell
    let stop: StopResource
    if isFiltering {
        stop = filteredStopResources[indexPath.row]
    } else {
        if (indexPath.section == 0 && favouriteStopResources.count > 0) {
            stop = favouriteStopResources[indexPath.row]
        }
        else {
            stop = otherStopResources[indexPath.row]
        }
    }
    cell.stopName?.text = stop.stopName
    cell.stopDistance?.text = ""
    return cell
  }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return isFiltering ? 1 : favouriteStopResources.count > 0 ? 2 : 1
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if (favouriteStopResources.count > 0 && !isFiltering) {
            switch section {
            case 0:
                return NSLocalizedString("FAVOURITE_STOPS", comment: "")
            case 1:
                return NSLocalizedString("OTHER_STOPS", comment: "")
            default:
                return nil
            }
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if (favouriteStopResources.count > 0 && !isFiltering) {
            switch section {
            case 0:
                (view as! UITableViewHeaderFooterView).contentView.backgroundColor = #colorLiteral(red: 0.9789999723, green: 0.7179999948, blue: 0.09799999744, alpha: 1)
                (view as! UITableViewHeaderFooterView).textLabel?.textColor = UIColor.white
            case 1:
                (view as! UITableViewHeaderFooterView).contentView.backgroundColor = #colorLiteral(red: 0.2910000086, green: 0.3569999933, blue: 0.5389999747, alpha: 1)
                (view as! UITableViewHeaderFooterView).textLabel?.textColor = UIColor.white
            default:
                break
            }
        }
        else {
            (view as! UITableViewHeaderFooterView).contentView.backgroundColor = #colorLiteral(red: 0.2910000086, green: 0.3569999933, blue: 0.5389999747, alpha: 1)
            (view as! UITableViewHeaderFooterView).textLabel?.textColor = UIColor.white
        }
    }
}

extension StopViewController: UISearchResultsUpdating {
  func updateSearchResults(for searchController: UISearchController) {
    let searchBar = searchController.searchBar
    filterContentForSearchText(searchBar.text!)
  }
}

extension StopViewController: UISearchBarDelegate {
  func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
    filterContentForSearchText(searchBar.text!)
  }
}
