//
//  DatePickerViewController.swift
//  menetrend-szekesfehervar
//
//  Created by Barnabas Lorincz on 2020. 02. 12..
//  Copyright © 2020. Barnabás Lőrincz. All rights reserved.
//

import UIKit

class DatePickerViewController: UIViewController {
    @IBOutlet weak var datePicker: UIDatePicker!
    
    var selectedDate: Date?
    var sender: DatePickerSender?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = NSLocalizedString("CHANGE_DATE", comment: "")
        datePicker.date = selectedDate!
    }
    @IBAction func doneButtonTapped(_ sender: Any) {
        switch self.sender {
        case .stopDetail:
            performSegue(withIdentifier: "applyStopDetailDateSelector", sender: self)
        case .routeDetail:
            performSegue(withIdentifier: "applyRouteDetailDateSelector", sender: self)
        default:
            return
        }
    }
    @IBAction func cancelButtonTapped(_ sender: Any) {
        switch self.sender {
        case .stopDetail:
            performSegue(withIdentifier: "cancelStopDetailDateSelector", sender: self)
        case .routeDetail:
            performSegue(withIdentifier: "cancelRouteDetailDateSelector", sender: self)
        default:
            return
        }
    }
    
}

