//
//  TripViewController.swift
//  menetrend-szekesfehervar
//
//  Created by Lőrincz Barnabás on 2020. 01. 18..
//  Copyright © 2020. Barnabás Lőrincz. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class TripTableViewCell: UITableViewCell {
    
    @IBOutlet weak var tripIcon: UIImageView!
    @IBOutlet weak var stopName: UILabel!
    @IBOutlet weak var arrivalTime: UILabel!
    
}

class TripViewController: UIViewController {
    @IBOutlet weak var tripTableView: UITableView!
    @IBOutlet weak var tripSegmentedControl: UISegmentedControl!
    @IBOutlet weak var tripMapView: MKMapView!
    
    var locationManager = CLLocationManager()
    
    var stopResources: [TripStopResource] = []
    var shapes : [ShapeResource] = []
    var tripAnnotations: [TripAnnotation] = []
    
    var tripId: String?
    var tripHeadsign: String?
    var routeShortName: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = tripHeadsign!
        tripSegmentedControl.setTitle(NSLocalizedString("MENU_STOPS", comment: ""), forSegmentAt: 0)
        tripSegmentedControl.setTitle(NSLocalizedString("MENU_MAP", comment: ""), forSegmentAt: 1)
        tripMapView.delegate = self
        tripTableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        loadStops()
        loadShapes()
        createPolyline()
        zoomOnRoute()
        updateView()
        tripSegmentedControl.addTarget(self, action: #selector(updateView), for: .valueChanged)
        initLocationManager()
    }
    
    func loadStops() {
        stopResources = TripDbHelper().getStopsForTripId(tripId: tripId!)
        tripTableView.reloadData()
    }
    
    func loadShapes() {
        shapes = TripDbHelper().getShapesForTripId(tripId: tripId!)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        for index in 1...stopResources.count {
            let stopResource = stopResources[index-1]
            tripAnnotations.append(TripAnnotation(title: dateFormatter.string(from: stopResource.arrivalTime), subtitle: stopResource.stopName, index: String(index), coordinate: CLLocationCoordinate2D(latitude: stopResource.stopLat, longitude: stopResource.stopLon)))
        }
        tripMapView.addAnnotations(tripAnnotations)
        tripMapView.register(TripAnnotationView.self,
        forAnnotationViewWithReuseIdentifier: MKMapViewDefaultAnnotationViewReuseIdentifier)
    }
    
    func createPolyline() {
        var locations: [CLLocationCoordinate2D] = []
        for shape in shapes {
            locations.append(CLLocationCoordinate2D(latitude: shape.shapeLat, longitude: shape.shapeLon))
        }
        let polyline = MKPolyline(coordinates: locations, count: locations.count)
        tripMapView.addOverlay(polyline)
    }
    
    func zoomOnRoute() {
        let startCoordinate = shapes[0]
        let finishCoordinate = shapes[shapes.count-1]
        let startLocation = CLLocation(latitude: startCoordinate.shapeLat, longitude: startCoordinate.shapeLon)
        let finishLocation = CLLocation(latitude: finishCoordinate.shapeLat, longitude: finishCoordinate.shapeLon)
        let distanceInMeters = startLocation.distance(from: finishLocation)
        let midPointLat = (startCoordinate.shapeLat + finishCoordinate.shapeLat) / 2
        let midPointLong = (startCoordinate.shapeLon + finishCoordinate.shapeLon) / 2
        let midPointLocation = CLLocationCoordinate2D(latitude: midPointLat, longitude: midPointLong)
        let region = MKCoordinateRegion(center: midPointLocation, latitudinalMeters: CLLocationDistance(exactly: distanceInMeters + 1000)!, longitudinalMeters: CLLocationDistance(exactly: distanceInMeters + 1000)!)
        tripMapView.setRegion(tripMapView.regionThatFits(region), animated: true)
    }
    
    func zoomOnStop(stopCoordinates: CLLocationCoordinate2D) {
        let region = MKCoordinateRegion(center: stopCoordinates, latitudinalMeters: CLLocationDistance(exactly: 200)!, longitudinalMeters: CLLocationDistance(exactly: 200)!)
        tripMapView.setRegion(tripMapView.regionThatFits(region), animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
      super.viewWillAppear(animated)
    }

    @objc func updateView() {
        let idx = tripSegmentedControl.selectedSegmentIndex
        switch (idx) {
            case 0:
                tripTableView.isHidden = false
                tripMapView.isHidden = true
        case 1:
                tripTableView.isHidden = true
                tripMapView.isHidden = false
        default:
            tripTableView.isHidden = false
            tripMapView.isHidden = true
        }
        
    }

}

extension TripViewController: UITableViewDataSource {
  func tableView(_ tableView: UITableView,
                 numberOfRowsInSection section: Int) -> Int {
    return stopResources.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      let cell = tableView.dequeueReusableCell(withIdentifier: "LabelCell", for: indexPath)
                          as! TripTableViewCell
    let stop = stopResources[indexPath.row]
    cell.stopName?.text = stop.stopName
    let formatter = DateFormatter()
    formatter.dateFormat = "HH:mm"
    let arrivalTime = formatter.string(from: stop.arrivalTime)
    cell.arrivalTime?.text = arrivalTime
    switch (indexPath.row) {
        case 0:
            cell.tripIcon.image = #imageLiteral(resourceName: "trip_start.png")
            break
        case stopResources.count-1:
            cell.tripIcon.image = #imageLiteral(resourceName: "trip_end.png")
            break
        default:
            cell.tripIcon.image = #imageLiteral(resourceName: "trip_middle.png")
            break
    }
    return cell
  }
    
}

extension TripViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        zoomOnStop(stopCoordinates: tripAnnotations[indexPath.row].coordinate)
        tripSegmentedControl.selectedSegmentIndex = 1
        updateView()
    }
    
}

extension TripViewController: MKMapViewDelegate {
  
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if (overlay is MKPolyline) {
            let polylineRenderer = MKPolylineRenderer(overlay: overlay)
            polylineRenderer.strokeColor = #colorLiteral(red: 0.8130000234, green: 0.1529999971, blue: 0.2230000049, alpha: 1).withAlphaComponent(0.5)
            polylineRenderer.lineWidth = 5
            return polylineRenderer
        }
        return MKOverlayRenderer()
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
      guard let annotation = annotation as? TripAnnotation else { return nil }
      let identifier = "marker"
      var view: MKMarkerAnnotationView
      if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
        as? MKMarkerAnnotationView {
        dequeuedView.annotation = annotation
        view = dequeuedView
        view.canShowCallout = true
        view.calloutOffset = CGPoint(x: 0, y: 0)
        view.glyphText = annotation.index
        view.markerTintColor = #colorLiteral(red: 0.2910000086, green: 0.3569999933, blue: 0.5389999747, alpha: 1)
        view.displayPriority = MKFeatureDisplayPriority.required
      } else {
        view = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: identifier)
        view.canShowCallout = true
        view.calloutOffset = CGPoint(x: 0, y: 0)
        view.glyphText = annotation.index
        view.markerTintColor = #colorLiteral(red: 0.2910000086, green: 0.3569999933, blue: 0.5389999747, alpha: 1)
        view.displayPriority = MKFeatureDisplayPriority.required
      }
      return view
    }
    
}

extension TripViewController: CLLocationManagerDelegate {
    
    func checkLocationAuthorizationStatus() {
      if CLLocationManager.authorizationStatus() == .authorizedWhenInUse || CLLocationManager.authorizationStatus() == .authorizedAlways {
        tripMapView.showsUserLocation = true
      } else {
        locationManager.requestWhenInUseAuthorization()
      }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            tripMapView.showsUserLocation = true
        }
    }
    
    func initLocationManager() {
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
        checkLocationAuthorizationStatus()
    }
}
