//
//  TripTableViewController.swift
//  menetrend-szekesfehervar
//
//  Created by Lőrincz Barnabás on 2020. 01. 17..
//  Copyright © 2020. Barnabás Lőrincz. All rights reserved.
//

import UIKit

class RouteViewController: UIViewController {
    
    @IBOutlet var tableView: UITableView!
    var allRouteResources: [RouteResource] = []
    var otherRouteResources: [RouteResource] = []
    var favouriteRouteResources: [RouteResource] = []
    var filteredRouteResources: [RouteResource] = []
    let searchController = UISearchController(searchResultsController: nil)
    let favouritesService: FavouritesService = FavouritesService(key: "favouriteRoutes")

    override func viewDidLoad() {
        super.viewDidLoad()// Here goes the UIView for the header
        self.title = NSLocalizedString("MENU_ROUTES", comment: "")
        tableView.register(UINib(nibName: "RouteTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        
        // 1
        searchController.searchResultsUpdater = self as UISearchResultsUpdating
        // 2
        searchController.obscuresBackgroundDuringPresentation = false
        // 3
        searchController.searchBar.placeholder = NSLocalizedString("SEARCH_ROUTES", comment: "")
        // 4
        navigationItem.searchController = searchController
        // 5
        definesPresentationContext = true
        searchController.searchBar.delegate = self as UISearchBarDelegate
        
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(forName: UIResponder.keyboardWillChangeFrameNotification,
                                       object: nil, queue: .main) { (notification) in
                                        self.handleKeyboard(notification: notification) }
        notificationCenter.addObserver(forName: UIResponder.keyboardWillHideNotification,
                                       object: nil, queue: .main) { (notification) in
                                        self.handleKeyboard(notification: notification) }
        
    }
    
    func loadResources() {
        allRouteResources = RouteDbHelper().getRoutes()
        let favourites = favouritesService.getFavourites()
        favouriteRouteResources.removeAll()
        otherRouteResources.removeAll()
        for route in allRouteResources {
            if favourites.contains(route.routeShortName) {
                favouriteRouteResources.append(route)
                route.favourite = true
            }
            else {
                otherRouteResources.append(route)
                route.favourite = false
            }
        }
        tableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
      super.viewWillAppear(animated)
      loadResources()
      if let indexPath = tableView.indexPathForSelectedRow {
        tableView.deselectRow(at: indexPath, animated: true)
      }
    }
    
    var isSearchBarEmpty: Bool {
      return searchController.searchBar.text?.isEmpty ?? true
    }
    
    var isFiltering: Bool {
      let searchBarScopeIsFiltering = searchController.searchBar.selectedScopeButtonIndex != 0
      return searchController.isActive && (!isSearchBarEmpty || searchBarScopeIsFiltering)
    }
    
    func filterContentForSearchText(_ searchText: String) {
      filteredRouteResources = allRouteResources.filter { (routeResource: RouteResource) -> Bool in
        
        if isSearchBarEmpty {
            return true;
        } else {
            return routeResource.routeShortName.lowercased().contains(searchText.lowercased())
        }
      }
      tableView.reloadData()
    }
    
    func handleKeyboard(notification: Notification) {
      // 1
      guard notification.name == UIResponder.keyboardWillChangeFrameNotification else {
        view.layoutIfNeeded()
        return
      }
      
      guard
        let info = notification.userInfo,
        let keyboardFrame = info[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue
        else {
          return
      }
      
      // 2
        _ = keyboardFrame.cgRectValue.size.height
      UIView.animate(withDuration: 0.1, animations: { () -> Void in
        self.view.layoutIfNeeded()
      })
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
      guard
        segue.identifier == "ShowRouteDetail",
        let indexPath = tableView.indexPathForSelectedRow,
        let detailViewController = segue.destination as? RouteDetailViewController
        else {
          return
      }
        var route: RouteResource
        if isFiltering {
            route = filteredRouteResources[indexPath.row]
        } else {
            if (indexPath.section == 0 && favouriteRouteResources.count > 0) {
                route = favouriteRouteResources[indexPath.row]
            }
            else {
                route = otherRouteResources[indexPath.row]
            }
        }
        detailViewController.routeId = route.routeId
        detailViewController.routeShortName = route.routeShortName
        detailViewController.favourite = route.favourite
    }

}

extension RouteViewController: UITableViewDataSource, UITableViewDelegate {
  func tableView(_ tableView: UITableView,
                 numberOfRowsInSection section: Int) -> Int {
    if isFiltering {
      return filteredRouteResources.count
    }
    if (section == 0 && favouriteRouteResources.count > 0) {
        return favouriteRouteResources.count
    }
    else {
        return otherRouteResources.count
    }
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
                          as! RouteTableViewCell
    let route: RouteResource
    if isFiltering {
        route = filteredRouteResources[indexPath.row]
    } else {
        if (indexPath.section == 0 && favouriteRouteResources.count > 0) {
            route = favouriteRouteResources[indexPath.row]
        }
        else {
            route = otherRouteResources[indexPath.row]
        }
    }
    cell.title?.text = route.routeShortName
    cell.subtitle?.text = route.routeLongName
    cell.accessoryType = .disclosureIndicator
    return cell
  }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return isFiltering ? 1 : favouriteRouteResources.count > 0 ? 2 : 1
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            performSegue(withIdentifier: "ShowRouteDetail", sender: nil)
            tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if (favouriteRouteResources.count > 0 && !isFiltering) {
            switch section {
            case 0:
                return NSLocalizedString("FAVOURITE_ROUTES", comment: "")
            case 1:
                return NSLocalizedString("OTHER_ROUTES", comment: "")
            default:
                return nil
            }
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if (favouriteRouteResources.count > 0 && !isFiltering) {
            switch section {
            case 0:
                (view as! UITableViewHeaderFooterView).contentView.backgroundColor = #colorLiteral(red: 0.9789999723, green: 0.7179999948, blue: 0.09799999744, alpha: 1)
                (view as! UITableViewHeaderFooterView).textLabel?.textColor = UIColor.white
            case 1:
                (view as! UITableViewHeaderFooterView).contentView.backgroundColor = #colorLiteral(red: 0.2910000086, green: 0.3569999933, blue: 0.5389999747, alpha: 1)
                (view as! UITableViewHeaderFooterView).textLabel?.textColor = UIColor.white
            default:
                break
            }
        }
        else {
            (view as! UITableViewHeaderFooterView).contentView.backgroundColor = #colorLiteral(red: 0.2910000086, green: 0.3569999933, blue: 0.5389999747, alpha: 1)
            (view as! UITableViewHeaderFooterView).textLabel?.textColor = UIColor.white
        }
    }
}

extension RouteViewController: UISearchResultsUpdating {
  func updateSearchResults(for searchController: UISearchController) {
    let searchBar = searchController.searchBar
    filterContentForSearchText(searchBar.text!)
  }
}

extension RouteViewController: UISearchBarDelegate {
  func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
    filterContentForSearchText(searchBar.text!)
  }
}
