//
//  RouteDetailViewController.swift
//  menetrend-szekesfehervar
//
//  Created by Lőrincz Barnabás on 2020. 01. 18..
//  Copyright © 2020. Barnabás Lőrincz. All rights reserved.
//

import UIKit

class RouteDetailTableViewCell: UITableViewCell {
    
    @IBOutlet weak var tripHeadsign: UILabel!
    @IBOutlet weak var departureTime: UILabel!
}


class RouteDetailViewController: UIViewController {
    @IBOutlet weak var routeDetailTableView: UITableView!
    @IBOutlet weak var favouriteButton: UIBarButtonItem!
    
    @IBOutlet weak var dateButton: UIBarButtonItem!
    var tripResources: [TripResource] = []
    
    var routeId: String?
    var routeShortName: String?
    var favourite: Bool? {
        didSet {
            onFavouriteChanged()
        }
    }
    var selectedDate: Date = Date() {
        didSet {
            loadTrips()
        }
    }
    
    let favouritesService: FavouritesService = FavouritesService(key: "favouriteRoutes")
    var viewInitialized = false

    override func viewDidLoad() {
        super.viewDidLoad()
        dateButton.image = UIImage.fontAwesomeIcon(name: .calendar, style: .regular, textColor: .black, size: CGSize(width: 30, height: 30))
        viewInitialized = true
        self.title = routeShortName!
        onFavouriteChanged()
        loadTrips()
    }
    
    func loadTrips() {
        tripResources = TripDbHelper().getTripsForBusIdAndDay(routeId: routeId!, date: selectedDate)
        routeDetailTableView.reloadData()
        if (tripResources.count > 0) {
            routeDetailTableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: UITableView.ScrollPosition.top, animated: true)
        }
        else {
            showNoDataAlert()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if (segue.identifier == "ShowTripSegue") {
            guard
              let indexPath = routeDetailTableView.indexPathForSelectedRow,
              let detailViewController = segue.destination as? TripViewController
              else {
                return
            }
              let trip = tripResources[indexPath.row]
              detailViewController.tripId = trip.tripId
              detailViewController.tripHeadsign = trip.tripHeadSign
              detailViewController.routeShortName = routeShortName
          
          }
          if (segue.identifier == "RouteDetailToDatePickerSegue") {
              guard let detailNavigationController = segue.destination as? DatePickerNavigationController
                  else {
                    return
                    }
              guard let detailViewController = detailNavigationController.viewControllers.first as? DatePickerViewController
                  else {
                    return
                    }
                  detailViewController.selectedDate = selectedDate
                  detailViewController.sender = .routeDetail
            
            }
    }
    
    override func viewWillAppear(_ animated: Bool) {
      super.viewWillAppear(animated)
      if let indexPath = routeDetailTableView.indexPathForSelectedRow {
        routeDetailTableView.deselectRow(at: indexPath, animated: true)
      }
    }
    
    @IBAction func cancelRouteDetailDateSelector(_ segue: UIStoryboardSegue) {
    }
    
    @IBAction func applyRouteDetailDateSelector(_ segue: UIStoryboardSegue) {
        if let datePickerViewController = segue.source as? DatePickerViewController,
            let datePicker = datePickerViewController.datePicker {
            selectedDate = datePicker.date
        }
    }
    
    @IBAction func favouriteButtonClicked(_ sender: Any) {
        if (!(favourite ?? false)) {
            favouritesService.addToFavourites(favourite: routeShortName ?? "")
            favourite = true
        }
        else {
            favouritesService.removeFromFavourites(favourite: routeShortName ?? "")
            favourite = false
        }
    }
    
    func onFavouriteChanged() {
        if (viewInitialized) {
            favouriteButton.tintColor = #colorLiteral(red: 0.9789999723, green: 0.7179999948, blue: 0.09799999744, alpha: 1)
            if (favourite ?? true) {
                favouriteButton.image = UIImage.fontAwesomeIcon(name: .star, style: .solid, textColor: .black, size: CGSize(width: 30, height: 30))
            }
            else {
                favouriteButton.image = UIImage.fontAwesomeIcon(name: .star, style: .regular, textColor: .black, size: CGSize(width: 30, height: 30))
            }
        }
    }
    
    func showNoDataAlert() {
        DispatchQueue.main.async {
            let alertController = UIAlertController(title: NSLocalizedString("NO_DATA_TITLE_ROUTE", comment: ""), message: NSLocalizedString("NO_DATA_TEXT_ROUTE", comment: ""), preferredStyle: .alert)
            //We add buttons to the alert controller by creating UIAlertActions:
            let actionOk = UIAlertAction(title: "OK",
                style: .default,
                handler: nil) //You can use a block here to handle a press on this button

            alertController.addAction(actionOk)

            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension RouteDetailViewController: UITableViewDataSource, UITableViewDelegate {
  func tableView(_ tableView: UITableView,
                 numberOfRowsInSection section: Int) -> Int {
    return tripResources.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      let cell = tableView.dequeueReusableCell(withIdentifier: "RouteDetailCell", for: indexPath)
                          as! RouteDetailTableViewCell
    let trip = tripResources[indexPath.row]
    cell.tripHeadsign?.text = trip.tripHeadSign
    let formatter = DateFormatter()
    formatter.dateFormat = "HH:mm"
    let departureTime = formatter.string(from: trip.departureTime)
    cell.departureTime?.text = departureTime
    return cell
  }
  
  func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
      if (section == 0) {
          let dateFormatter = DateFormatter()
          dateFormatter.dateFormat = "yyyy. MMMM dd., eeee"
          return dateFormatter.string(from: selectedDate)
      }
      return ""
  }
  
  func numberOfSections(in tableView: UITableView) -> Int {
      return 1
  }
    
  func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
      (view as! UITableViewHeaderFooterView).contentView.backgroundColor = #colorLiteral(red: 0.2910000086, green: 0.3569999933, blue: 0.5389999747, alpha: 1)
      (view as! UITableViewHeaderFooterView).textLabel?.textColor = UIColor.white
  }
}
