//
//  HelpDetailViewController.swift
//  menetrend-szekesfehervar
//
//  Created by Barnabas Lorincz on 2020. 03. 26..
//  Copyright © 2020. Barnabás Lőrincz. All rights reserved.
//

import UIKit
import Down

class HelpDetailViewController: UIViewController {
    
    var content: String?

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        let downView = (try? DownView(frame: self.view.bounds, markdownString: content ?? "") {
            // Optional callback for loading finished
            })!
        self.view.addSubview(downView)
    }
    
    
    
    @IBAction func doneButtonTapped(_ sender: Any) {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
