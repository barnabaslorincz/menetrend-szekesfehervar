//
//  HelpViewController.swift
//  menetrend-szekesfehervar
//
//  Created by Barnabas Lorincz on 2020. 03. 19..
//  Copyright © 2020. Barnabás Lőrincz. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class HelpViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = NSLocalizedString("INFO_TITLE", comment: "")
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UINib(nibName: "SmallIconAndDescriptionTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        tableView.tableFooterView = UIView()
        
        NotificationSharingManager.sharedInstance.onNotificationRead?
        .observeOn(MainScheduler.instance) // make sure we're on main thread
        .subscribe { [weak self] newValue in
            self!.tableView.reloadData()
        }
        .disposed(by: disposeBag)

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension HelpViewController: UITableViewDataSource, UITableViewDelegate {
  func tableView(_ tableView: UITableView,
                 numberOfRowsInSection section: Int) -> Int {
    return infoModels.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
                          as! SmallIconAndDescriptionTableViewCell
    cell.imageIcon?.image = infoModels[indexPath.row].image
    cell.leftLabel?.text = infoModels[indexPath.row].leftLabel
    cell.rightLabel?.text = infoModels[indexPath.row].rightLabel
    cell.iconCount?.text = ""
    switch infoModels[indexPath.row].tapAction {
    case .text, .url:
        cell.accessoryType = .disclosureIndicator
        case .notifications:
            cell.accessoryType = .disclosureIndicator
            let notificationCount = NotificationDatabaseService.getUnreadNotificationCount()
            cell.iconCount?.text = String(notificationCount)
            if (notificationCount < 1) {
                cell.imageIcon.image = #imageLiteral(resourceName: "notification_done.png")
            }
    default:
        cell.accessoryType = .none
    }
    return cell
  }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch infoModels[indexPath.row].tapAction {
            case .text:
                showDetailWithContent(content: infoModels[indexPath.row].content)
            case .url:
                guard let url = URL(string: infoModels[indexPath.row].url) else { return }
                UIApplication.shared.open(url)
            case .notifications:
                performSegue(withIdentifier: "ShowNotitificationsSegue", sender: nil)
            default:
                break
            }
            tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func showDetailWithContent(content: String) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Help", bundle: nil)
        let helpDetailNavigationController = storyBoard.instantiateViewController(withIdentifier: "HelpNavigationController") as! HelpDetailNavigationController
        let helpDetailViewController = helpDetailNavigationController.children.first as! HelpDetailViewController
        helpDetailViewController.content = content
        self.navigationController?.present(helpDetailNavigationController, animated: true, completion: nil)
    }
}

var infoModels: [InfoModel] = [
    InfoModel(leftLabel: NSLocalizedString("NOTIFICATIONS", comment: ""), rightLabel: "", image:#imageLiteral(resourceName: "notification.png"), url: "", content: "", tapAction: .notifications),
    InfoModel(leftLabel: NSLocalizedString("VOLANBUSZ_URL", comment: ""), rightLabel: "", image: #imageLiteral(resourceName: "globe"), url: "https://www.volanbusz.hu/hu/menetrendek/helyi-jaratok/szekesfehervar", content: "", tapAction: .url),
    InfoModel(leftLabel: NSLocalizedString("SZEKESFEHERVAR_URL", comment: ""), rightLabel: "", image: #imageLiteral(resourceName: "globe"), url: "https://www.szekesfehervar.hu/buszmenetrend", content: "", tapAction: .url),
    InfoModel(leftLabel: NSLocalizedString("VERISON_LABEL", comment: ""), rightLabel: Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? "", image: #imageLiteral(resourceName: "version"), url: "", content: "", tapAction: .nothing),
    InfoModel(leftLabel: NSLocalizedString("LAST_UPDATED_LABEL", comment: ""), rightLabel: "2020-05-08", image: #imageLiteral(resourceName: "last_updated"), url: "", content: "", tapAction: .nothing),
    InfoModel(leftLabel: NSLocalizedString("DATA_PROTECTION_LABEL", comment: ""), rightLabel: "", image: #imageLiteral(resourceName: "privacy"), url: "", content: NSLocalizedString("DATA_PROTECTION_TEXT", comment: ""), tapAction: .text),
    InfoModel(leftLabel: NSLocalizedString("LICENCE_LABEL", comment: ""), rightLabel: "", image: #imageLiteral(resourceName: "licence"), url: "", content: NSLocalizedString("LICENCE_TEXT", comment: ""), tapAction: .text),
    InfoModel(leftLabel: NSLocalizedString("DEVELOPER_LABEL", comment: ""), rightLabel: NSLocalizedString("DEVELOPER_NAME", comment: ""), image: #imageLiteral(resourceName: "developer"), url: "", content: "", tapAction: .nothing),
    InfoModel(leftLabel: NSLocalizedString("CONTACT_DEVELOPER_LABEL", comment: ""), rightLabel: "", image: #imageLiteral(resourceName: "contact_developer"), url: "https://lorinczbarnabas.hu/menetrend", content: "", tapAction: .url),
    
]
