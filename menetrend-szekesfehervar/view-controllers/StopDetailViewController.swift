//
//  StopDetailViewController.swift
//  menetrend-szekesfehervar
//
//  Created by Barnabas Lorincz on 2020. 02. 05..
//  Copyright © 2020. Barnabás Lőrincz. All rights reserved.
//

import UIKit

class StopDetailTableCell: UITableViewCell {
    @IBOutlet weak var routeShortName: UILabel!
    @IBOutlet weak var tripHeadsign: UILabel!
    @IBOutlet weak var departureTime: UILabel!
}

class StopDetailViewController: UIViewController {
    @IBOutlet weak var stopTripTableView: UITableView!
    
    var stopTripResources: [StopTripResource] = []
    
    var stopId: String?
    var stopName: String?
    var sender: StopDetailSender = .none
    var selectedDate: Date = Date() {
        didSet {
            loadData()
        }
    }
    @IBOutlet weak var favouriteButton: UIBarButtonItem!
    var favourite: Bool? {
        didSet {
            onFavouriteChanged()
        }
    }
    @IBOutlet weak var dateButton: UIBarButtonItem!
    
    
    let favouritesService: FavouritesService = FavouritesService(key: "favouriteStops")
    var viewInitialized = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewInitialized = true
        dateButton.image = UIImage.fontAwesomeIcon(name: .calendar, style: .regular, textColor: .black, size: CGSize(width: 30, height: 30))
        self.title = stopName
        onFavouriteChanged()
        loadData()
        // Do any additional setup after loading the view.
    }
    
    func loadData() {
        switch sender {
            case .list:
                loadTripsForStopName()
            case .map:
                loadTripsForStopId()
            default:
                return
        }
    }
    
    func loadTripsForStopName() {
        stopTripResources = StopDbHelper().getTripsForStopName(stopName: stopName!, date: selectedDate)
        stopTripTableView.reloadData()
        if (stopTripResources.count > 0) {
            stopTripTableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: UITableView.ScrollPosition.top, animated: true)
        }
        else {
            showNoDataAlert()
        }
    }
    
    func loadTripsForStopId() {
        stopTripResources = StopDbHelper().getTripsForStopId(stopId: stopId!, date: Date())
        stopTripTableView.reloadData()
        if (stopTripResources.count > 0) {
            stopTripTableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: UITableView.ScrollPosition.top, animated: true)
        }
        else {
            showNoDataAlert()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
      super.viewWillAppear(animated)
      if let indexPath = stopTripTableView.indexPathForSelectedRow {
        stopTripTableView.deselectRow(at: indexPath, animated: true)
      }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
      if (segue.identifier == "ShowTripSegue2") {
              guard let indexPath = stopTripTableView.indexPathForSelectedRow,
              let detailViewController = segue.destination as? TripViewController
              else {
                return
                }
              let trip = stopTripResources[indexPath.row]
              detailViewController.tripId = trip.tripId
              detailViewController.tripHeadsign = trip.tripHeadsign
              detailViewController.routeShortName = trip.routeShortName
        
        }
        if (segue.identifier == "StopDetailToDatePickerSegue") {
            guard let detailNavigationController = segue.destination as? DatePickerNavigationController
                else {
                  return
                  }
            guard let detailViewController = detailNavigationController.viewControllers.first as? DatePickerViewController
                else {
                  return
                  }
                detailViewController.selectedDate = selectedDate
                detailViewController.sender = .stopDetail
          }
    }
    
    @IBAction func cancelStopDetailDateSelector(_ segue: UIStoryboardSegue) {
    }
    
    @IBAction func applyStopDetailDateSelector(_ segue: UIStoryboardSegue) {
        if let datePickerViewController = segue.source as? DatePickerViewController,
            let datePicker = datePickerViewController.datePicker {
            selectedDate = datePicker.date
        }
    }
    
    @IBAction func favouriteButtonClicked(_ sender: Any) {
        if (!(favourite ?? false)) {
            favouritesService.addToFavourites(favourite: stopName ?? "")
            favourite = true
        }
        else {
            favouritesService.removeFromFavourites(favourite: stopName ?? "")
            favourite = false
        }
    }
    
    func onFavouriteChanged() {
        if (viewInitialized && sender == .list) {
            favouriteButton.tintColor = #colorLiteral(red: 0.9789999723, green: 0.7179999948, blue: 0.09799999744, alpha: 1)
            if (favourite ?? true) {
                favouriteButton.image = UIImage.fontAwesomeIcon(name: .star, style: .solid, textColor: .black, size: CGSize(width: 30, height: 30))
            }
            else {
                favouriteButton.image = UIImage.fontAwesomeIcon(name: .star, style: .regular, textColor: .black, size: CGSize(width: 30, height: 30))
            }
        }
    }
    
    func showNoDataAlert() {
        DispatchQueue.main.async {
            let alertController = UIAlertController(title: NSLocalizedString("NO_DATA_TITLE_STOP", comment: ""), message: NSLocalizedString("NO_DATA_TEXT_STOP", comment: ""), preferredStyle: .alert)
            //We add buttons to the alert controller by creating UIAlertActions:
            let actionOk = UIAlertAction(title: "OK",
                style: .default,
                handler: nil) //You can use a block here to handle a press on this button

            alertController.addAction(actionOk)

            self.present(alertController, animated: true, completion: nil)
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension StopDetailViewController: UITableViewDataSource, UITableViewDelegate {
  func tableView(_ tableView: UITableView,
                 numberOfRowsInSection section: Int) -> Int {
    return stopTripResources.count
  }
      
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        (view as! UITableViewHeaderFooterView).contentView.backgroundColor = #colorLiteral(red: 0.2910000086, green: 0.3569999933, blue: 0.5389999747, alpha: 1)
        (view as! UITableViewHeaderFooterView).textLabel?.textColor = UIColor.white
    }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      let cell = tableView.dequeueReusableCell(withIdentifier: "StopDetailCell", for: indexPath)
                          as! StopDetailTableCell
    let stopTrip = stopTripResources[indexPath.row]
    cell.tripHeadsign?.text = stopTrip.tripHeadsign
    cell.routeShortName?.text = stopTrip.routeShortName
    let formatter = DateFormatter()
    formatter.dateFormat = "HH:mm"
    let departureTime = formatter.string(from: stopTrip.departureTime)
    cell.departureTime?.text = departureTime
    return cell
  }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if (section == 0) {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy. MMMM dd., eeee"
            return dateFormatter.string(from: selectedDate)
        }
        return ""
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
}
