//
//  MapViewController.swift
//  menetrend-szekesfehervar
//
//  Created by Lőrincz Barnabás on 2019. 12. 24..
//  Copyright © 2019. Barnabás Lőrincz. All rights reserved.
//

import UIKit
import MapKit
import Foundation
import CoreLocation

class MapViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {
    @IBOutlet weak var locationButton: UIBarButtonItem!
    @IBOutlet weak var homeButton: UIBarButtonItem!
    @IBOutlet weak var outOfServiceAreaLabel: UILabel!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var serviceAreaLabel: UILabel!
    var mapStopModels: [StopAnnotation] = []
    var locationManager = CLLocationManager()
    var userLocationUpdated = false
    var serviceArea = ServiceAreaResource.empty()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = NSLocalizedString("MENU_MAP", comment: "")
        serviceAreaLabel.text = NSLocalizedString("OUT_OF_SERVICE_AREA", comment: "")
        mapView.delegate = self
        homeButton.image = UIImage.fontAwesomeIcon(name: .home, style: .solid, textColor: .black, size: CGSize(width: 30, height: 30))
        locationButton.image = UIImage.fontAwesomeIcon(name: .locationArrow, style: .solid, textColor: .black, size: CGSize(width: 30, height: 30))
        initLocationManager()
        loadInitialData()
        loadServiceArea()
        mapView.register(StopAnnotationView.self,
        forAnnotationViewWithReuseIdentifier: MKMapViewDefaultAnnotationViewReuseIdentifier)
        mapView.register(StopClusterAnnotationView.self, forAnnotationViewWithReuseIdentifier: MKMapViewDefaultClusterAnnotationViewReuseIdentifier)
        mapView.isRotateEnabled = false
        mapView.addAnnotations(mapStopModels)
    }
    
    func centerMapOnLocation(location: CLLocationCoordinate2D) {
        let region = MKCoordinateRegion(center: location, latitudinalMeters: CLLocationDistance(exactly: 400)!, longitudinalMeters: CLLocationDistance(exactly: 400)!)
        mapView.setRegion(mapView.regionThatFits(region), animated: true)
    }
    
    func loadInitialData() {
        mapStopModels = MapDbHelper().getStops()
    }
    
    func loadServiceArea() {
        serviceArea = MapDbHelper().getServiceArea()
        checkServiceArea()
    }
    
    func showUserLocationButton() {
        locationButton.isEnabled = true
        locationButton.image = UIImage.fontAwesomeIcon(name: .locationArrow, style: .solid, textColor: .black, size: CGSize(width: 30, height: 30))
    }
    
    func checkServiceArea() {
        let topLeft1 = mapView.convert(CGPoint(x: 0, y: 0), toCoordinateFrom: mapView)
        let bottomRightPoint = CGPoint(x: mapView.frame.width, y: mapView.frame.height)
        let bottomRight1 = mapView.convert(bottomRightPoint, toCoordinateFrom: mapView)
        let topLeft2 = CLLocationCoordinate2D(latitude: serviceArea.maxLat, longitude: serviceArea.minLon)
        let bottomRight2 = CLLocationCoordinate2D(latitude: serviceArea.minLat, longitude: serviceArea.maxLon)
        if (!doOverlap(l1: topLeft1, r1: bottomRight1, l2: topLeft2, r2: bottomRight2)) {
            outOfServiceAreaLabel.isHidden = false
        }
        else {
            outOfServiceAreaLabel.isHidden = true
        }
    }
    
    func doOverlap(l1: CLLocationCoordinate2D, r1: CLLocationCoordinate2D, l2: CLLocationCoordinate2D, r2: CLLocationCoordinate2D) -> Bool {
        // If one rectangle is on left side of other
        if (l1.longitude > r2.longitude || l2.longitude > r1.longitude) {
            return false
        }
        // If one rectangle is above other
        if (l1.latitude < r2.latitude || l2.latitude < r1.latitude) {
            return false
        }
        return true
    }
    
    func checkLocationAuthorizationStatus() {
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
        mapView.showsUserLocation = true
        showUserLocationButton()
        followUserTrackingMode()
      } else {
        // let initialLocation = CLLocationCoordinate2D(latitude: 47.1885062, longitude: 18.4075793)
        // centerMapOnLocation(location: initialLocation)
        zoomOnCity()
        locationManager.requestWhenInUseAuthorization()
        locationButton.image = UIImage.fontAwesomeIcon(name: .locationArrow, style: .solid, textColor: .black, size: CGSize(width: 30, height: 30))
      }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
          mapView.showsUserLocation = true
          showUserLocationButton()
          followUserTrackingMode()
        }
        else {
            locationButton.image = UIImage.fontAwesomeIcon(name: .locationArrow, style: .solid, textColor: .black, size: CGSize(width: 30, height: 30))
        }
    }
    
    func initLocationManager() {
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
        checkLocationAuthorizationStatus()
    }
    
    func mapViewDidChangeVisibleRegion(_ mapView: MKMapView) {
        checkServiceArea()
        noneTrackingMode()
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        guard let annotation = view.annotation as? StopAnnotation else {return}
        performSegue(withIdentifier: "MapToStopDetailSegue", sender: annotation)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
      guard
        segue.identifier == "MapToStopDetailSegue",
        let detailViewController = segue.destination as? StopDetailViewController,
        let annotation = sender as? StopAnnotation
        else {
          return
      }
        detailViewController.stopId = annotation.discipline
        detailViewController.stopName = annotation.title
        detailViewController.sender = .map
    }
    @IBAction func locationButtonTapped(_ sender: Any) {
        switch(mapView.userTrackingMode) {
        case .none:
            followUserTrackingMode()
        case .follow:
            noneTrackingMode()
        default:
            return
        }
    }
    @IBAction func homeButtonTapped(_ sender: Any) {
        noneTrackingMode()
        zoomOnCity()
    }
    
    func zoomOnCity() {
        let topLeft = CLLocation(latitude: serviceArea.maxLat, longitude: serviceArea.minLon)
        let bottomRight = CLLocation(latitude: serviceArea.minLat, longitude: serviceArea.maxLon)
        let distanceInMeters = topLeft.distance(from: bottomRight)
        let midPointLat = (topLeft.coordinate.latitude + bottomRight.coordinate.latitude) / 2
        let midPointLong = (topLeft.coordinate.longitude + bottomRight.coordinate.longitude) / 2
        let midPointLocation = CLLocationCoordinate2D(latitude: midPointLat, longitude: midPointLong)
        let region = MKCoordinateRegion(center: midPointLocation, latitudinalMeters: CLLocationDistance(exactly: distanceInMeters + 1000)!, longitudinalMeters: CLLocationDistance(exactly: distanceInMeters + 1000)!)
        mapView.setRegion(mapView.regionThatFits(region), animated: true)
    }
    
    func followUserTrackingMode() {
        mapView.setUserTrackingMode(.follow, animated: true)
        
    }
    
    func noneTrackingMode() {
        mapView.setUserTrackingMode(.none, animated: true)
    }

    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard let annotation = annotation as? StopAnnotation else { return nil }
        return StopAnnotationView(annotation: annotation, reuseIdentifier: StopAnnotationView.ReuseID)
    }
}
