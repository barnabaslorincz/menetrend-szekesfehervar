//
//  NotificationDetailViewController.swift
//  menetrend-szekesfehervar
//
//  Created by Lőrincz Barnabás on 2020. 05. 07..
//  Copyright © 2020. Barnabás Lőrincz. All rights reserved.
//

import UIKit
import Down
import RealmSwift
import RxSwift

class NotificationDetailViewController: UIViewController {
    
    var content: String?
    var notificationId: String?


    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        let downView = (try? DownView(frame: self.view.bounds, markdownString: content ?? "") {
            // Optional callback for loading finished
            })!
        self.view.addSubview(downView)
    }
    
    @IBAction func doneButtonTapped(_ sender: Any) {
        let realm = try! Realm()
        let predicate = NSPredicate(format: "id = %@", notificationId!)
        let notification = realm.objects(NotificationDatabaseItem.self).filter(predicate)
            try! realm.write {
                notification.first?.setValue(true, forKey: "read")
            }
        NotificationSharingManager.sharedInstance.triggerNotificationUpdate(newValue: "Updated")
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
