//
//  NotificationListViewController.swift
//  menetrend-szekesfehervar
//
//  Created by Lőrincz Barnabás on 2020. 05. 07..
//  Copyright © 2020. Barnabás Lőrincz. All rights reserved.
//

import UIKit
import RealmSwift
import RxSwift

class NotificationListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var tableView: UITableView!
    var notifications: [NotificationDatabaseItem] = []
    let dateFormatter = DateFormatter()
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = NSLocalizedString("NOTIFICATIONS", comment: "")
        dateFormatter.dateFormat = "yyyy. MM. dd., EEEE HH:mm"
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UINib(nibName: "NotificationTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        loadNotifications()
        NotificationSharingManager.sharedInstance.onNotificationRead?
        .observeOn(MainScheduler.instance) // make sure we're on main thread
        .subscribe { [weak self] newValue in
            self!.loadNotifications()
        }
        .disposed(by: disposeBag)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadNotifications()
    }
    
    func loadNotifications() {
        let realm = try! Realm()
        notifications = Array(realm.objects(NotificationDatabaseItem.self).sorted(byKeyPath: "dateadded", ascending: false))
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notifications.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
                              as! NotificationTableViewCell
        let notification = notifications[indexPath.row]
        cell.title?.text = notification.title
        cell.subtitle?.text = dateFormatter.string(from: notification.dateadded)
        cell.accessoryType = .disclosureIndicator
        cell.imageIcon.image = notification.read ? #imageLiteral(resourceName: "notification_read.png") : #imageLiteral(resourceName: "notification_unread.png")
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            showDetailWithContent(notification: notifications[indexPath.row])
            tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func showDetailWithContent(notification: NotificationDatabaseItem) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Help", bundle: nil)
        let notificationDetailNavigationController = storyBoard.instantiateViewController(withIdentifier: "NotificationDetailNavigationController") as! NotificationDetailNavigationController
        let notificationDetailViewController = notificationDetailNavigationController.children.first as! NotificationDetailViewController
        notificationDetailViewController.content = notification.text
        notificationDetailViewController.notificationId = notification.id
        self.navigationController?.present(notificationDetailNavigationController, animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
