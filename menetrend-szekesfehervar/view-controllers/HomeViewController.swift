//
//  HomeViewController.swift
//  menetrend-szekesfehervar
//
//  Created by Lőrincz Barnabás on 2019. 12. 22..
//  Copyright © 2019. Barnabás Lőrincz. All rights reserved.
//

import UIKit
import RealmSwift
import RxSwift
import RxCocoa

class HomeViewController: UITabBarController {

    private var observer: NSObjectProtocol?
    
    var db: DBHelper = DBHelper()
    let notificationService: NotificationService = NotificationService()
    @IBOutlet weak var mainTabBar: UITabBar!
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpIcons()
        NotificationSharingManager.sharedInstance.onNotificationRead?
        .observeOn(MainScheduler.instance) // make sure we're on main thread
        .subscribe { [weak self] newValue in
            self!.checkNewNotifications()
        }
        .disposed(by: disposeBag)
        observer = NotificationCenter.default.addObserver(forName: UIApplication.willEnterForegroundNotification, object: nil, queue: .main) { [unowned self] notification in
            self.getNotifications()
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let notFirstRun = UserDefaults.standard.bool(forKey: "notFirstRun")
        if (!notFirstRun) {
            // any code that should be run once
            UserDefaults.standard.set(true, forKey: "notFirstRun")
        }
    }
    
    func setUpIcons() {
        let mapTabBarItem = (self.tabBar.items?[0])! as UITabBarItem
        mapTabBarItem.image = UIImage.fontAwesomeIcon(name: .map, style: .solid, textColor: .black, size: CGSize(width: 30, height: 30))
        mapTabBarItem.selectedImage = UIImage.fontAwesomeIcon(name: .map, style: .solid, textColor: .black, size: CGSize(width: 30, height: 30))
        mapTabBarItem.title = NSLocalizedString("MENU_MAP", comment: "")

        let stopTabBarItem = (self.tabBar.items?[1])! as UITabBarItem
        stopTabBarItem.image = UIImage.fontAwesomeIcon(name: .mapSigns, style: .solid, textColor: .black, size: CGSize(width: 30, height: 30))
        stopTabBarItem.selectedImage = UIImage.fontAwesomeIcon(name: .mapSigns, style: .solid, textColor: .black, size: CGSize(width: 30, height: 30))
        stopTabBarItem.title = NSLocalizedString("MENU_STOPS", comment: "")


        let tripTabBarItem = (self.tabBar.items?[2])! as UITabBarItem
        tripTabBarItem.image = UIImage.fontAwesomeIcon(name: .bus, style: .solid, textColor: .black, size: CGSize(width: 30, height: 30))
        tripTabBarItem.selectedImage = UIImage.fontAwesomeIcon(name: .bus, style: .solid, textColor: .black, size: CGSize(width: 30, height: 30))
        tripTabBarItem.title = NSLocalizedString("MENU_ROUTES", comment: "")

        let helpTabBarItem = (self.tabBar.items?[3])! as UITabBarItem
        helpTabBarItem.image = UIImage.fontAwesomeIcon(name: .infoCircle, style: .solid, textColor: .black, size: CGSize(width: 30, height: 30))
        helpTabBarItem.selectedImage = UIImage.fontAwesomeIcon(name: .infoCircle, style: .solid, textColor: .black, size: CGSize(width: 30, height: 30))
        helpTabBarItem.title = NSLocalizedString("MENU_HELP", comment: "")
    
    }
    
    func getNotifications() {
        notificationService.getNotifications(lastKnownDate: NotificationDatabaseService.getLastKnownDate(), completion: { [weak self] result in
            switch result {
            case .success(let notifications):
                self!.addNewNotifications(notifications: notifications)
            case .failure(_):
                self!.checkNewNotifications()
            }
        })
    }
    
    func addNewNotifications(notifications: [NotificationItem]) {
        let queue = DispatchQueue(label: "Serial queue")
        let group = DispatchGroup()
        group.enter()
        queue.async {
            autoreleasepool {
                NotificationDatabaseService.addNewNotifications(notifications: notifications)
                group.leave()
            }
        }
        group.notify(queue: .main)  {
            NotificationSharingManager.sharedInstance.triggerNotificationUpdate(newValue: "Updated")
        }
    }
    
    func checkNewNotifications() {
        let unreadCount = NotificationDatabaseService.getUnreadNotificationCount()
        let helpTabBarItem = (self.tabBar.items?[3])! as UITabBarItem
        if (unreadCount > 0) {
            helpTabBarItem.badgeValue = String(unreadCount)
        }
        else {
            helpTabBarItem.badgeValue = nil
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getNotifications()
    }
    
    deinit {
        if let observer = observer {
            NotificationCenter.default.removeObserver(observer)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
