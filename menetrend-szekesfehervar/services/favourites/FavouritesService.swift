//
//  FavouritesService.swift
//  menetrend-szekesfehervar
//
//  Created by Lőrincz Barnabás on 2020. 05. 03..
//  Copyright © 2020. Barnabás Lőrincz. All rights reserved.
//

import Foundation


class FavouritesService {
    
    let key: String
    
    init(key: String) {
        self.key = key
    }
    
    func getFavourites() -> [String] {
        return UserDefaults.standard.stringArray(forKey: self.key) ?? []
    }
    
    func addToFavourites(favourite: String) {
        var favourites = getFavourites()
        if (favourites.contains(favourite)) {
            return
        }
        else {
            favourites.append(favourite)
            UserDefaults.standard.set(favourites, forKey: self.key)
        }
    }
    
    func removeFromFavourites(favourite: String) {
        var favourites = getFavourites()
        if (!favourites.contains(favourite)) {
            return
        }
        else {
            favourites.remove(at: favourites.firstIndex(of: favourite)!)
            UserDefaults.standard.set(favourites, forKey: self.key)
        }
    }
    
    
}
