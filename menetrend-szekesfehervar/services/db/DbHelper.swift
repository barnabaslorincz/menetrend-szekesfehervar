//
//  DbHelper.swift
//  menetrend-szekesfehervar
//
//  Created by Lőrincz Barnabás on 2020. 01. 15..
//  Copyright © 2020. Barnabás Lőrincz. All rights reserved.
//

import Foundation
import SQLite3

class DBHelper
{
    init()
    {
        openDatabase()
    }
    
    var db:OpaquePointer?

    func openDatabase()
    {
        let filemanager = FileManager.default

        let documentDirectoryPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString
        let destinationPath = documentDirectoryPath.appendingPathComponent("menetrend_2020.db")
        let sourcePath = Bundle.main.path(forResource: "menetrend_2020", ofType: "db")
        
        if !filemanager.fileExists(atPath: destinationPath) {
            do {
               try filemanager.copyItem(atPath: sourcePath!, toPath: destinationPath)
            }
            catch let error1 as NSError{ print (" OOPS Something went wrong : \(error1)")}
        }



        let returnCode = sqlite3_open_v2(destinationPath, &db, SQLITE_OPEN_READONLY, nil)
        if SQLITE_OK != returnCode {
            let errmsg = String(cString: sqlite3_errmsg(db))
            fatalError("Failed to open db: \(errmsg)")
        }
    }
    
    func closeDb() {
        _ = sqlite3_close(db)
    }
    
}
