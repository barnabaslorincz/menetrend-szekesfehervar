//
//  TripDbHelper.swift
//  menetrend-szekesfehervar
//
//  Created by Lőrincz Barnabás on 2020. 01. 19..
//  Copyright © 2020. Barnabás Lőrincz. All rights reserved.
//

import Foundation
import SQLite3

class TripDbHelper {
    
    let db: OpaquePointer?
    let dbHelper: DBHelper
    
    init() {
        dbHelper = DBHelper()
        db = dbHelper.db
    }
    
    func getTripsForBusIdAndDay(routeId: String, date: Date) -> [TripResource] {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US")
        dateFormatter.dateFormat = "yyyMMdd"
        let formattedDate = dateFormatter.string(from: date)
        dateFormatter.dateFormat = "EEEE"
        let formattedDay = dateFormatter.string(from: date).lowercased()
        let queryStatementString = (
            "SELECT Trips.trip_id,Trips.trip_headsign,Trips.shape_id,StopTimes.departure_time from trips left join CalendarDates on trips.service_id = CalendarDates.service_id and CalendarDates.date = " + formattedDate + " inner join calendar on calendar.service_id = trips.service_id inner join StopTimes on StopTimes.trip_id = trips.trip_id where StopTimes.stop_sequence = 0 and trips.route_id=" + routeId + " and Calendar.start_date <= " + formattedDate + " and Calendar.end_date >= " + formattedDate + " and ((calendar." + formattedDay + " = 1 and CalendarDates.exception_type is null) or CalendarDates.exception_type = 1) order by StopTimes.departure_time ASC"
            ).cString(using: .utf8)!
        var queryStatement: OpaquePointer? = nil
        var trips : [TripResource] = []
        if sqlite3_prepare_v2(db, queryStatementString, -1, &queryStatement, nil) == SQLITE_OK {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "HH:mm:ss"
            while sqlite3_step(queryStatement) == SQLITE_ROW {
                let tripId = String(describing: String(cString: sqlite3_column_text(queryStatement, 0)))
                let tripHeadsign = String(describing: String(cString: sqlite3_column_text(queryStatement, 1)))
                let shapeId = sqlite3_column_int(queryStatement, 2)
                let departureTimeRaw = String(describing: String(cString: sqlite3_column_text(queryStatement, 3)))
                let departureTime = dateFormatter.date(from:departureTimeRaw)!
                trips.append(TripResource(tripId: tripId, tripHeadSign: tripHeadsign, shapeId: Int(shapeId), departureTime: departureTime))
            }
        } else {
            print("SELECT statement could not be prepared")
        }
        sqlite3_finalize(queryStatement)
        dbHelper.closeDb()
        return trips
    }
    
    func getStopsForTripId(tripId: String) -> [TripStopResource] {
        let queryStatementString = ("Select StopTimes.stop_id, stop_name, arrival_time, stops.stop_lat, stops.stop_lon from trips inner join StopTimes on Trips.trip_id = stopTimes.trip_id inner join stops on stops.stop_id = StopTimes.stop_id where Trips.trip_id = " + tripId + " order by arrival_time asc").cString(using: .utf8)!
        var queryStatement: OpaquePointer? = nil
        var stops : [TripStopResource] = []
        if sqlite3_prepare_v2(db, queryStatementString, -1, &queryStatement, nil) == SQLITE_OK {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "HH:mm:ss"
            while sqlite3_step(queryStatement) == SQLITE_ROW {
                let stopId = String(describing: String(cString: sqlite3_column_text(queryStatement, 0)))
                let stopName = String(describing: String(cString: sqlite3_column_text(queryStatement, 1)))
                let arrivalTimeRaw = String(describing: String(cString: sqlite3_column_text(queryStatement, 2)))
                let stopLat = sqlite3_column_double(queryStatement, 3)
                let stopLon = sqlite3_column_double(queryStatement, 4)
                let arrivalTime = dateFormatter.date(from:arrivalTimeRaw)!
                stops.append(TripStopResource(stopId: stopId, stopName: stopName, arrivalTime: arrivalTime, stopLat: stopLat, stopLon: stopLon))
            }
        } else {
            print("SELECT statement could not be prepared")
        }
        sqlite3_finalize(queryStatement)
        dbHelper.closeDb()
        return stops
    }
    
    func getShapesForTripId(tripId: String) -> [ShapeResource] {
        let queryStatementString = ("Select shape_pt_lat, shape_pt_lon from shapes where shape_id in (select DISTINCT shape_id from trips where trip_id = " +  tripId + ")").cString(using: .utf8)!
        var queryStatement: OpaquePointer? = nil
        var shapes : [ShapeResource] = []
        if sqlite3_prepare_v2(db, queryStatementString, -1, &queryStatement, nil) == SQLITE_OK {
            while sqlite3_step(queryStatement) == SQLITE_ROW {
                let shapeLat = sqlite3_column_double(queryStatement, 0)
                let shapeLon = sqlite3_column_double(queryStatement, 1)
                shapes.append(ShapeResource(shapeLat: shapeLat, shapeLon: shapeLon))
            }
        } else {
            print("SELECT statement could not be prepared")
        }
        sqlite3_finalize(queryStatement)
        dbHelper.closeDb()
        return shapes
    }
    
}
