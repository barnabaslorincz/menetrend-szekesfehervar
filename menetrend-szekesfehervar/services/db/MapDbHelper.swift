//
//  MapDbHelper.swift
//  menetrend-szekesfehervar
//
//  Created by Lőrincz Barnabás on 2020. 01. 17..
//  Copyright © 2020. Barnabás Lőrincz. All rights reserved.
//

import Foundation
import SQLite3
import CoreLocation

class MapDbHelper {
    
    let db: OpaquePointer?
    let dbHelper: DBHelper
    
    init() {
        dbHelper = DBHelper()
        db = dbHelper.db
    }
    
    func getStops() -> [StopAnnotation] {
        let queryStatementString = "SELECT Stops.stop_id, Stops.stop_name, group_concat(DISTINCT route_short_name ), Stops.stop_lat, Stops.stop_lon from Stops inner join StopTimes on Stops.stop_id = StopTimes.stop_id inner join Trips on StopTimes.trip_id = Trips.trip_id inner join Routes where trips.route_id = routes.route_id group by Stops.stop_id;".cString(using: .utf8)!
        var queryStatement: OpaquePointer? = nil
        var stops: [StopAnnotation] = []
        if sqlite3_prepare_v2(db, queryStatementString, -1, &queryStatement, nil) == SQLITE_OK {
            while sqlite3_step(queryStatement) == SQLITE_ROW {
                let stopId = String(describing: String(cString: sqlite3_column_text(queryStatement, 0)))
                let stopName = String(describing: String(cString: sqlite3_column_text(queryStatement, 1)))
                let buses = String(describing: String(cString: sqlite3_column_text(queryStatement, 2)))
                let stopLat = sqlite3_column_double(queryStatement, 3)
                let stopLon = sqlite3_column_double(queryStatement, 4)
                stops.append(StopAnnotation(title: stopName, locationName: stopName, discipline: stopId, coordinate: CLLocationCoordinate2D(latitude: stopLat, longitude: stopLon), buses: buses))
            }
        } else {
            print("SELECT statement could not be prepared")
        }
        sqlite3_finalize(queryStatement)
        dbHelper.closeDb()
        return stops
    }
    
    func getServiceArea() -> ServiceAreaResource {
        let queryStatementString = "select  max(stop_lat), min(stop_lon), min(stop_lat), max(stop_lon) from stops;".cString(using: .utf8)!
        var queryStatement: OpaquePointer? = nil
        var serviceArea: ServiceAreaResource = ServiceAreaResource(maxLat: 0, minLon: 0, minLat: 0, maxLon: 0)
        if sqlite3_prepare_v2(db, queryStatementString, -1, &queryStatement, nil) == SQLITE_OK {
            while sqlite3_step(queryStatement) == SQLITE_ROW {
                let maxLat = sqlite3_column_double(queryStatement, 0)
                let minLon = sqlite3_column_double(queryStatement, 1)
                let minLat = sqlite3_column_double(queryStatement, 2)
                let maxLon = sqlite3_column_double(queryStatement, 3)
                serviceArea = ServiceAreaResource(maxLat: maxLat, minLon: minLon, minLat: minLat, maxLon: maxLon)
            }
        } else {
            print("SELECT statement could not be prepared")
        }
        sqlite3_finalize(queryStatement)
        dbHelper.closeDb()
        return serviceArea
    }
    
}
