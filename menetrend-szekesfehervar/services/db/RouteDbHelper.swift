//
//  RouteDbHelper.swift
//  menetrend-szekesfehervar
//
//  Created by Lőrincz Barnabás on 2020. 01. 16..
//  Copyright © 2020. Barnabás Lőrincz. All rights reserved.
//

import Foundation
import SQLite3

class RouteDbHelper {
    
    let db: OpaquePointer?
    let dbHelper: DBHelper
    
    init() {
        dbHelper = DBHelper()
        db = dbHelper.db
    }
    
    func getRoutes() -> [RouteResource] {
        let queryStatementString = "SELECT * FROM Routes;".cString(using: .utf8)!
        var queryStatement: OpaquePointer? = nil
        var routes : [RouteResource] = []
        if sqlite3_prepare_v2(db, queryStatementString, -1, &queryStatement, nil) == SQLITE_OK {
            while sqlite3_step(queryStatement) == SQLITE_ROW {
                let routeId = String(describing: String(cString: sqlite3_column_text(queryStatement, 0)))
                let routeShortName = String(describing: String(cString: sqlite3_column_text(queryStatement, 1)))
                let routeLongName = String(describing: String(cString: sqlite3_column_text(queryStatement, 2)))
                routes.append(RouteResource(routeShortName: routeShortName, routeLongName: routeLongName, routeId: routeId, favourite: false))
            }
        } else {
            print("SELECT statement could not be prepared")
        }
        sqlite3_finalize(queryStatement)
        dbHelper.closeDb()
        return routes
    }
    
}
