//
//  StopDbHelper.swift
//  menetrend-szekesfehervar
//
//  Created by Barnabas Lorincz on 2020. 01. 30..
//  Copyright © 2020. Barnabás Lőrincz. All rights reserved.
//

import Foundation
import SQLite3

class StopDbHelper {
    
    let db: OpaquePointer?
    let dbHelper: DBHelper
    
    init() {
        dbHelper = DBHelper()
        db = dbHelper.db
    }
    
    func getAllStops() -> [StopResource] {
        let queryStatementString = "SELECT stop_name From Stops GROUP BY stop_name ORDER by stop_name ASC;".cString(using: .utf8)!
        var queryStatement: OpaquePointer? = nil
        var stops : [StopResource] = []
        if sqlite3_prepare_v2(db, queryStatementString, -1, &queryStatement, nil) == SQLITE_OK {
            while sqlite3_step(queryStatement) == SQLITE_ROW {
                let stopName = String(describing: String(cString: sqlite3_column_text(queryStatement, 0)))
                stops.append(StopResource(stopName: stopName, latitude: 0, longitude: 0, distance: 0, stopId: "", favourite: false))
            }
        } else {
            print("SELECT statement could not be prepared")
        }
        sqlite3_finalize(queryStatement)
        dbHelper.closeDb()
        return stops
    }
    
    func getTripsForStopId(stopId: String, date: Date) -> [StopTripResource] {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US")
        dateFormatter.dateFormat = "yyyMMdd"
        let formattedDate = dateFormatter.string(from: date)
        let queryStatementString = ("SELECT routes.route_short_name, Trips.trip_id,Trips.trip_headsign, StopTimes.departure_time from trips left join CalendarDates on trips.service_id = CalendarDates.service_id and CalendarDates.date = " + formattedDate + " inner join calendar on calendar.service_id = trips.service_id inner join StopTimes on StopTimes.trip_id = trips.trip_id inner join routes on routes.route_id = trips.route_id where StopTimes.stop_id = \"" + stopId + "\" and Calendar.start_date <= " + formattedDate + " and Calendar.end_date >= " + formattedDate + " and ((calendar.tuesday = 1 and CalendarDates.exception_type is null) or CalendarDates.exception_type = 1) order by StopTimes.departure_time ASC;").cString(using: .utf8)!
        var queryStatement: OpaquePointer? = nil
        var stopTrips : [StopTripResource] = []
        if sqlite3_prepare_v2(db, queryStatementString, -1, &queryStatement, nil) == SQLITE_OK {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "HH:mm:ss"
            while sqlite3_step(queryStatement) == SQLITE_ROW {
                let routeShortName = String(describing: String(cString: sqlite3_column_text(queryStatement, 0)))
                let tripId = String(describing: String(cString: sqlite3_column_text(queryStatement, 1)))
                let tripHeadsign = String(describing: String(cString: sqlite3_column_text(queryStatement, 2)))
                let departureTimeRaw = String(describing: String(cString: sqlite3_column_text(queryStatement, 3)))
                let departureTime = dateFormatter.date(from:departureTimeRaw)!
                stopTrips.append(StopTripResource(routeShortName: routeShortName, tripId: tripId, tripHeadsign: tripHeadsign, departureTime: departureTime))
            }
        } else {
            print("SELECT statement could not be prepared")
        }
        sqlite3_finalize(queryStatement)
        dbHelper.closeDb()
        return stopTrips
    }
    
    func getTripsForStopName(stopName: String, date: Date) -> [StopTripResource] {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US")
        dateFormatter.dateFormat = "yyyMMdd"
        let formattedDate = dateFormatter.string(from: date)
        let queryStatementString = ("SELECT routes.route_short_name, Trips.trip_id,Trips.trip_headsign, StopTimes.departure_time from trips left join CalendarDates on trips.service_id = CalendarDates.service_id and CalendarDates.date = " + formattedDate + " inner join calendar on calendar.service_id = trips.service_id inner join StopTimes on StopTimes.trip_id = trips.trip_id inner join routes on routes.route_id = trips.route_id left join Stops on  StopTimes.stop_id = Stops.stop_id where Stops.stop_name = \"" + stopName + "\" and Calendar.start_date <= " + formattedDate + " and Calendar.end_date >= " + formattedDate + " and ((calendar.tuesday = 1 and CalendarDates.exception_type is null) or CalendarDates.exception_type = 1) order by StopTimes.departure_time ASC;").cString(using: .utf8)!
        var queryStatement: OpaquePointer? = nil
        var stopTrips : [StopTripResource] = []
        if sqlite3_prepare_v2(db, queryStatementString, -1, &queryStatement, nil) == SQLITE_OK {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "HH:mm:ss"
            while sqlite3_step(queryStatement) == SQLITE_ROW {
                let routeShortName = String(describing: String(cString: sqlite3_column_text(queryStatement, 0)))
                let tripId = String(describing: String(cString: sqlite3_column_text(queryStatement, 1)))
                let tripHeadsign = String(describing: String(cString: sqlite3_column_text(queryStatement, 2)))
                let departureTimeRaw = String(describing: String(cString: sqlite3_column_text(queryStatement, 3)))
                let departureTime = dateFormatter.date(from:departureTimeRaw)!
                stopTrips.append(StopTripResource(routeShortName: routeShortName, tripId: tripId, tripHeadsign: tripHeadsign, departureTime: departureTime))
            }
        } else {
            print("SELECT statement could not be prepared")
        }
        sqlite3_finalize(queryStatement)
        dbHelper.closeDb()
        return stopTrips
    }
    
}
