//
//  NotificationItem.swift
//  menetrend-szekesfehervar
//
//  Created by Lőrincz Barnabás on 2020. 05. 07..
//  Copyright © 2020. Barnabás Lőrincz. All rights reserved.
//

import Foundation

struct NotificationItem: Codable {
    let id: String
    let title: String
    let text: String
    let dateadded: String
    let validuntil: String
}
