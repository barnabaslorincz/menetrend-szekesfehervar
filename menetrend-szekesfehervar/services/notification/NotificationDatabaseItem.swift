//
//  NotificationDatabaseItem.swift
//  menetrend-szekesfehervar
//
//  Created by Lőrincz Barnabás on 2020. 05. 07..
//  Copyright © 2020. Barnabás Lőrincz. All rights reserved.
//

import RealmSwift

class NotificationDatabaseItem: Object {
    @objc dynamic var id: String = ""
    @objc dynamic var title: String = ""
    @objc dynamic var text: String = ""
    @objc dynamic var dateadded: Date = Date()
    @objc dynamic var validuntil: Date = Date()
    @objc dynamic var read: Bool = false
}
