//
//  NotificationsService.swift
//  menetrend-szekesfehervar
//
//  Created by Lőrincz Barnabás on 2020. 05. 07..
//  Copyright © 2020. Barnabás Lőrincz. All rights reserved.
//

import Alamofire

class NotificationService {

    enum GetNotificationsFailureReason: Int, Error {
        case unAuthorized = 401
        case notFound = 404
    }

    typealias GetNotificationsResult = Result<[NotificationItem], GetNotificationsFailureReason>
    typealias GetNotificationsCompletion = (_ result: GetNotificationsResult) -> Void

    func getNotifications(lastKnownDate: Date, completion: @escaping GetNotificationsCompletion) {
        let dateFormatter = DateFormatter()
        let formatter = ISO8601DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let requestUrl = "https://lorinczbarnabas.hu/notification-api/notification/read_new.php?datefrom=" + formatter.string(from: lastKnownDate)
        AF.request(requestUrl)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success:
                    do {
                        guard let data = response.data else {
                            completion(Result.failure(.notFound))
                            return
                        }

                        let notifications = try JSONDecoder().decode([NotificationItem].self, from: data)
                        completion(Result.success(notifications))
                    } catch {
                        completion(Result.failure(.notFound))
                    }
                case .failure(_):
                    if let statusCode = response.response?.statusCode,
                        let reason = GetNotificationsFailureReason(rawValue: statusCode) {
                        completion(.failure(reason))
                    }
                    completion(Result.failure(.notFound))
                }
        }
    }

}
