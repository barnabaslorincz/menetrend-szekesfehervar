//
//  NotificationSharingManager.swift
//  menetrend-szekesfehervar
//
//  Created by Lőrincz Barnabás on 2020. 05. 07..
//  Copyright © 2020. Barnabás Lőrincz. All rights reserved.
//

import RxSwift

class NotificationSharingManager {
    static let sharedInstance = NotificationSharingManager()

    private let _notificationRead = PublishSubject<String>()
    let onNotificationRead: Observable<String>? // any object can subscribe to text change using this observable

    // call this method whenever you need to change text
    func triggerNotificationUpdate(newValue: String) {
        _notificationRead.onNext(newValue)
    }

    init() {
        onNotificationRead = _notificationRead.share(replay: 1)
    }
}
