//
//  NotificationDatabaseService.swift
//  menetrend-szekesfehervar
//
//  Created by Lőrincz Barnabás on 2020. 05. 07..
//  Copyright © 2020. Barnabás Lőrincz. All rights reserved.
//

import Foundation
import RealmSwift

class NotificationDatabaseService {
    
    static func addNewNotifications(notifications: [NotificationItem]) {
            let realm = try! Realm()
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            for notification in notifications {
                let notif = NotificationDatabaseItem();
                notif.id = notification.id
                notif.title = notification.title
                notif.text = notification.text
                notif.dateadded = dateFormatter.date(from: notification.dateadded)!
                notif.validuntil = dateFormatter.date(from: notification.validuntil)!
                let predicate = NSPredicate(format: "id = %@", notification.id)
                let found = realm.objects(NotificationDatabaseItem.self).filter(predicate)
                try! realm.write {
                    if (found.first != nil) {
                        if (found.first!.dateadded != notif.dateadded) {
                            realm.delete(found.first!)
                            realm.add(notif)
                        }
                    }
                    else {
                        realm.add(notif)
                    }
                }
            }
    }
    
    static func getUnreadNotificationCount() -> Int {
        let realm = try! Realm()
        return realm.objects(NotificationDatabaseItem.self).filter("read == false").count
    }
    
    static func getLastKnownDate() -> Date {
        let realm = try! Realm()
        let result = realm.objects(NotificationDatabaseItem.self).sorted(byKeyPath: "dateadded", ascending: false)
        if (result.first != nil) {
            return result.first!.dateadded
        }
        return Date(timeIntervalSince1970: 0)
    }
    
}
