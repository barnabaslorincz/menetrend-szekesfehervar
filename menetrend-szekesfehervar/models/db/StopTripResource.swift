//
//  StopTripResource.swift
//  menetrend-szekesfehervar
//
//  Created by Barnabas Lorincz on 2020. 02. 05..
//  Copyright © 2020. Barnabás Lőrincz. All rights reserved.
//

import Foundation

class StopTripResource {
        
    let routeShortName: String
    let tripId: String
    let tripHeadsign: String
    let departureTime: Date
    
    
    init(routeShortName: String, tripId: String, tripHeadsign: String, departureTime: Date) {
        self.routeShortName = routeShortName
        self.tripId = tripId
        self.tripHeadsign = tripHeadsign
        self.departureTime = departureTime
    }
}
