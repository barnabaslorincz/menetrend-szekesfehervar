//
//  ShapeResource.swift
//  menetrend-szekesfehervar
//
//  Created by Barnabas Lorincz on 2020. 02. 06..
//  Copyright © 2020. Barnabás Lőrincz. All rights reserved.
//

import Foundation

class ShapeResource {
    
    let shapeLat: Double
    let shapeLon: Double
    
    init(shapeLat: Double, shapeLon: Double) {
        self.shapeLat = shapeLat
        self.shapeLon = shapeLon
    }
}
