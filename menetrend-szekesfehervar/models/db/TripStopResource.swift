//
//  StopResource.swift
//  menetrend-szekesfehervar
//
//  Created by Lőrincz Barnabás on 2020. 01. 21..
//  Copyright © 2020. Barnabás Lőrincz. All rights reserved.
//

import Foundation

class TripStopResource {
        
    let stopId: String
    let stopName: String
    let arrivalTime: Date
    let stopLat: Double
    let stopLon: Double
    
    
    init(stopId: String, stopName: String, arrivalTime: Date, stopLat: Double, stopLon: Double) {
        self.stopId = stopId
        self.stopName = stopName
        self.arrivalTime = arrivalTime
        self.stopLat = stopLat
        self.stopLon = stopLon
    }
}
