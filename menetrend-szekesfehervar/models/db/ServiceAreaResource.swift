//
//  ServiceAreaResource.swift
//  menetrend-szekesfehervar
//
//  Created by Barnabas Lorincz on 2020. 02. 08..
//  Copyright © 2020. Barnabás Lőrincz. All rights reserved.
//

import Foundation

class ServiceAreaResource {
    
    let maxLat: Double
    let minLon: Double
    let minLat: Double
    let maxLon: Double
    
    
    init(maxLat: Double, minLon: Double, minLat: Double, maxLon: Double) {
        self.maxLat = maxLat
        self.minLon = minLon
        self.minLat = minLat
        self.maxLon = maxLon
    }
    
    static func empty() -> ServiceAreaResource {
        return ServiceAreaResource(maxLat: 0, minLon: 0, minLat: 0, maxLon: 0)
    }
}
