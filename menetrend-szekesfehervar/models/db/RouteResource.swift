//
//  TripResource.swift
//  menetrend-szekesfehervar
//
//  Created by Lőrincz Barnabás on 2020. 01. 16..
//  Copyright © 2020. Barnabás Lőrincz. All rights reserved.
//

import Foundation

class RouteResource {
        
    let routeShortName: String
    let routeLongName: String
    let routeId: String
    var favourite: Bool
    
    init(routeShortName: String, routeLongName: String, routeId: String, favourite: Bool) {
        self.routeLongName = routeLongName
        self.routeShortName = routeShortName
        self.routeId = routeId
        self.favourite = favourite
    }
}
