//
//  StopResource.swift
//  menetrend-szekesfehervar
//
//  Created by Barnabas Lorincz on 2020. 01. 30..
//  Copyright © 2020. Barnabás Lőrincz. All rights reserved.
//

import Foundation

class StopResource {
        
    let stopName: String
    let latitude: Double
    let longitude: Double
    let distance: Int
    let stopId: String
    var favourite: Bool
    
    init(stopName: String, latitude: Double, longitude: Double, distance: Int, stopId: String, favourite: Bool) {
        self.stopName = stopName
        self.latitude = latitude
        self.longitude = longitude
        self.distance = distance
        self.stopId = stopId
        self.favourite = favourite
    }
    
    
}
