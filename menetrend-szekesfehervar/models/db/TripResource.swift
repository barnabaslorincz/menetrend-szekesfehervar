//
//  TripResource.swift
//  menetrend-szekesfehervar
//
//  Created by Lőrincz Barnabás on 2020. 01. 19..
//  Copyright © 2020. Barnabás Lőrincz. All rights reserved.
//

import Foundation

class TripResource {
        
    let tripId: String
    let tripHeadSign: String
    let shapeId: Int
    let departureTime: Date
    
    
    init(tripId: String, tripHeadSign: String, shapeId: Int, departureTime: Date) {
        self.tripId = tripId
        self.tripHeadSign = tripHeadSign
        self.shapeId = shapeId
        self.departureTime = departureTime
    }
}
