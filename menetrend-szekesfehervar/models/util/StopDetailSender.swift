//
//  StopDetailSender.swift
//  menetrend-szekesfehervar
//
//  Created by Barnabas Lorincz on 2020. 02. 09..
//  Copyright © 2020. Barnabás Lőrincz. All rights reserved.
//

import Foundation

enum StopDetailSender {
    case none, map, list
}
