//
//  DatePickerSender.swift
//  menetrend-szekesfehervar
//
//  Created by Barnabas Lorincz on 2020. 02. 19..
//  Copyright © 2020. Barnabás Lőrincz. All rights reserved.
//

import Foundation

enum DatePickerSender {
    case stopDetail, routeDetail
}
