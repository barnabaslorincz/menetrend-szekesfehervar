//
//  MapStopModel.swift
//  menetrend-szekesfehervar
//
//  Created by Lőrincz Barnabás on 2019. 12. 24..
//  Copyright © 2019. Barnabás Lőrincz. All rights reserved.
//

import Foundation
import MapKit

class StopAnnotation: NSObject, MKAnnotation {
      let title: String?
      let locationName: String
      let discipline: String
      let coordinate: CLLocationCoordinate2D
    let buses: String
      
      init(title: String, locationName: String, discipline: String, coordinate: CLLocationCoordinate2D, buses: String) {
        self.title = title
        self.locationName = locationName
        self.discipline = discipline
        self.coordinate = coordinate
        self.buses = buses
        
        super.init()
      }
      
      var subtitle: String? {
        return buses.replacingOccurrences(of: ",", with: ", ")
    }
}
