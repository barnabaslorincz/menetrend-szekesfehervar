//
//  TripAnnotation.swift
//  menetrend-szekesfehervar
//
//  Created by Barnabas Lorincz on 2020. 02. 07..
//  Copyright © 2020. Barnabás Lőrincz. All rights reserved.
//

import Foundation
import MapKit

class TripAnnotation: NSObject, MKAnnotation {
      let title: String?
      let subtitle: String?
      let index: String
      let coordinate: CLLocationCoordinate2D
      
      init(title: String, subtitle: String, index: String, coordinate: CLLocationCoordinate2D) {
        self.title = title
        self.subtitle = subtitle
        self.index = index
        self.coordinate = coordinate
        
        super.init()
      }
    
    var markerTintColor: UIColor  {
        return .blue
    }
}
