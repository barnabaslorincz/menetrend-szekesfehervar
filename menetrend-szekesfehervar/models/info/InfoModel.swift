//
//  InfoModel.swift
//  menetrend-szekesfehervar
//
//  Created by Barnabas Lorincz on 2020. 03. 21..
//  Copyright © 2020. Barnabás Lőrincz. All rights reserved.
//

import Foundation
import UIKit

class InfoModel {
    
    var leftLabel: String
    var rightLabel: String
    var image: UIImage
    var url: String
    var content: String
    var tapAction: TapAction
    
    init(leftLabel: String, rightLabel: String, image: UIImage, url: String, content: String, tapAction: TapAction) {
        self.leftLabel = leftLabel
        self.rightLabel = rightLabel
        self.image = image
        self.url = url
        self.content = content
        self.tapAction = tapAction
    }
    
}

enum TapAction {
    case nothing, url, text, notifications
}
