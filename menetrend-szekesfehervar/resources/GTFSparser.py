import json

sqlLines = []
#
# sqlLines.append("CREATE TABLE FeedInfo;")
# sqlLines.append("\n") nem kell
# sqlLines.append("CREATE TABLE Agency;")
# sqlLines.append("\n") nem kell
#

sqlLines.append("DROP TABLE IF EXISTS CalendarDates;")
sqlLines.append("\n")

sqlLines.append("DROP TABLE IF EXISTS Calendar;")
sqlLines.append("\n")

sqlLines.append("DROP TABLE IF EXISTS Routes;")
sqlLines.append("\n")

sqlLines.append("DROP TABLE IF EXISTS Shapes;")
sqlLines.append("\n")

sqlLines.append("DROP TABLE IF EXISTS StopTimes;")
sqlLines.append("\n")

sqlLines.append("DROP TABLE IF EXISTS Stops;")
sqlLines.append("\n")

sqlLines.append("DROP TABLE IF EXISTS Trips;")
sqlLines.append("\n")


sqlLines.append("CREATE TABLE CalendarDates(date DATE, service_id INTEGER, exception_type INTEGER);")
sqlLines.append("\n")

sqlLines.append("CREATE TABLE Calendar(service_id INTEGER, start_date DATE, end_date DATE, monday INTEGER, tuesday INTEGER, wednesday INTEGER, thursday INTEGER, friday INTEGER, saturday INTEGER, sunday INTEGER);")
sqlLines.append("\n")
#
sqlLines.append("CREATE TABLE Routes(route_id VARCHAR(255), route_short_name VARCHAR(255), route_long_name VARCHAR(255), route_type VARCHAR(255), route_color VARCHAR(255), route_text_color VARCHAR(255));")
sqlLines.append("\n")
#
sqlLines.append("CREATE TABLE Shapes(shape_id INTEGER, shape_pt_lat REAL, shape_pt_lon REAL, shape_pt_sequence INTEGER);")
sqlLines.append("\n")

sqlLines.append("CREATE TABLE StopTimes(trip_id VARCHAR(255), stop_id VARCHAR(255), arrival_time TIME, departure_time TIME, stop_sequence VARCHAR(255), stop_headsign VARCHAR(255));")
sqlLines.append("\n")
#
sqlLines.append("CREATE TABLE Stops(stop_id VARCHAR(255), platform_code VARCHAR(255), stop_lat REAL, stop_lon REAL, stop_name VARCHAR(255), wheelchair_boarding BOOLEAN);")
sqlLines.append("\n")
#
sqlLines.append("CREATE TABLE Trips(trip_id VARCHAR(255), route_id VARCHAR(255), service_id INTEGER, trip_headsign VARCHAR(255), direction_id INTEGER, shape_id INTEGER);")
sqlLines.append("\n")


# sqlLines.append("ALTER TABLE StopTimes ADD COLUMN trip_id VARCHAR(255);")
# sqlLines.append("\n")
# sqlLines.append("ALTER TABLE StopTimes ADD COLUMN stop_id VARCHAR(255);")
# sqlLines.append("\n")
# sqlLines.append("ALTER TABLE StopTimes ADD COLUMN arrival_time TIME;")
# sqlLines.append("\n")
# sqlLines.append("ALTER TABLE StopTimes ADD COLUMN departure_time TIME;")
# sqlLines.append("\n")
# sqlLines.append("ALTER TABLE StopTimes ADD COLUMN stop_sequence VARCHAR(255);")
# sqlLines.append("\n")
# sqlLines.append("ALTER TABLE StopTimes ADD COLUMN stop_headsign VARCHAR(255);")
# sqlLines.append("\n")

i = 1
with open('stop_times.txt') as my_file2:
    for line in my_file2:
        if i != 1:
            if "\"" in line:
                split2 = line.split("\"")
                split2[1] = split2[1].replace(",", " /")
                line = "".join(split2)
            split = line.split(",")
            sqlLines.append("INSERT INTO StopTimes VALUES (\""+ split[0] + "\",\"" + split[1] + "\",\"" + split[2] + "\",\"" + split[3] + "\",\"" + split[4] + "\",\"" + split[5] + "\");")
            sqlLines.append("\n")
        i = i+1

i = 1
with open('calendar.txt') as my_file3:
    for line in my_file3:
        if i != 1:
            split = line.split(",")
            sqlLines.append("INSERT INTO Calendar VALUES (\""+ split[0] + "\"," + split[1] + "," + split[2] + "," + split[3] + "," + split[4] + "," + split[5] + "," + split[6] + "," + split[7] + "," + split[8] + "," + split[9] + ");")
            sqlLines.append("\n")
        i = i+1

i = 1
with open('calendar_dates.txt') as my_file4:
    for line in my_file4:
        if i != 1:
            line = line.replace("\"", "")
            split = line.split(",")
            sqlLines.append("INSERT INTO CalendarDates VALUES (\""+ split[0] + "\"," + split[1] + "," + split[2] + ");")
            sqlLines.append("\n")
        i = i+1

i = 1
with open('routes.txt') as my_filex:
    for line in my_filex:
        if i != 1:
            if "\"" in line:
                split2 = line.split("\"")
                split2[1] = split2[1].replace(",", " /")
                line = "".join(split2)
            split = line.split(",")
            sqlLines.append("INSERT INTO Routes VALUES (\""+ split[0] + "\",\"" + split[1] + "\",\"" + split[2] + "\",\"" + split[3] + "\",\"" + split[4] + "\",\"" + split[5] + "\");")
            sqlLines.append("\n")
        i = i+1

i = 1
with open('shapes.txt') as my_file5:
    for line in my_file5:
        if i != 1:
            if "\"" in line:
                split2 = line.split("\"")
                split2[1] = split2[1].replace(",", " /")
                line = "".join(split2)
            split = line.split(",")
            sqlLines.append("INSERT INTO Shapes VALUES ("+ split[0] + "," + split[1] + "," + split[2] + "," + split[3] + ");")
            sqlLines.append("\n")
        i = i+1

i = 1
with open('stops.txt') as my_file6:
    for line in my_file6:
        if i != 1:
            if "\"" in line:
                split2 = line.split("\"")
                split2[1] = split2[1].replace(",", " /")
                line = "".join(split2)
            split = line.split(",")
            sqlLines.append("INSERT INTO Stops VALUES (\""+ split[0] + "\",\"" + split[1] + "\"," + split[2]+ "," + split[3] + ",\"" + split[4] + "\"," + split[5] + ");")
            sqlLines.append("\n")
        i = i+1

i = 1
with open('trips.txt') as my_file7:
    for line in my_file7:
        if i != 1:
            split = line.split(",")
            sqlLines.append("INSERT INTO Trips VALUES (\""+ split[0] + "\",\"" + split[1] + "\"," + split[2] + ",\"" + split[3] + "\"," + split[4] + "," + split[5] + ");")
            sqlLines.append("\n")
        i = i+1


file1 = open("insert.sql","w")

file1.writelines(sqlLines)
file1.close()
