import json

stopTimes = []
i = 1
with open('stop_times.txt') as my_file2:
    for line in my_file2:
        if i != 1:
            stopTimes.append({
                "trip_id": line.split(",")[0],
                "stop_id": line.split(",")[1]
            })
        i = i+1

trips = []
i = 1
with open('trips.txt') as my_file3:
    for line in my_file3:
        if i != 1:
            trips.append({
                "trip_id": line.split(",")[0],
                "route_id": line.split(",")[1]
            })
        i = i+1

routes = []
i = 1
with open('routes.txt') as my_file3:
    for line in my_file3:
        if i != 1:
            routes.append({
                "route_id": line.split(",")[0],
                "route_short_name": line.split(",")[1]
            })
        i = i+1

stops = []
i = 1
with open('stops.txt') as my_file:
    for line in my_file:
        if i != 1:
            stop_id = line.split(",")[0]
            buses = []
            trip_ids = []
            route_ids = []
            for stopTime in stopTimes:
                if stopTime["stop_id"] == stop_id:
                    trip_ids.append(stopTime["trip_id"])
            for trip_id in trip_ids:
                for trip in trips:
                    if trip_id == trip["trip_id"]:
                        route_ids.append(trip["route_id"])
            for route in routes:
                for route_id in route_ids:
                    if route["route_id"] == route_id:
                        if route["route_short_name"] not in buses:
                            buses.append(route["route_short_name"])
            stops.append({
                "stopId": stop_id,
                "stopLatitude": line.split(",")[2],
                "stopLongitude": line.split(",")[3],
                "stopName": line.split(",")[4],
                "platformCode": line.split(",")[1],
                "buses": buses,
            })
        i = i+1

print(stops)
with open('map_data.json', 'w') as outfile:
    json.dump(stops, outfile)
